# Override audio volume steps to 25 steps
PRODUCT_PROPERTY_OVERRIDES += \
    ro.config.vc_call_vol_steps=25 \
    ro.config.media_vol_steps=25
